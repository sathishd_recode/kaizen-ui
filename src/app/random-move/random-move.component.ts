import { Component, Input, OnInit, ViewChild } from '@angular/core';

import { ChartComponent,
  ApexNonAxisChartSeries, ApexResponsive, ApexChart, ApexLegend ,
  ApexAxisChartSeries, ApexFill, ApexXAxis, ApexYAxis, ApexTooltip, ApexTitleSubtitle } from 'ng-apexcharts';
  
export type BarChartRMoveOptions = {
  series : ApexAxisChartSeries,
  chart : ApexChart,
  xaxis : ApexXAxis,
  yaxis : ApexYAxis | ApexYAxis [],
  title : ApexTitleSubtitle,
  labels : string[],
  dataLabels : any,
  stroke : any,
  fill : ApexFill,
  tooltip : ApexTooltip
}
@Component({
  selector: 'app-random-move',
  templateUrl: './random-move.component.html',
  styleUrls: ['./random-move.component.css']
})
export class RandomMoveComponent implements OnInit {
@Input() data = []
@Input() lineData = [];
@Input() labelData = []
 @ViewChild("iChart", {static : false}) iChart:ChartComponent;
public barChartRMoveOptions : Partial<BarChartRMoveOptions>;
  constructor() {
    
      console.log(this.data, this.lineData, this.labelData)  
    
    
    this.barChartRMoveOptions = {
      series: [
        {
          name : "Random Move",
          type : "column",
          data : this.data
        },
        {
          name : "Random Move - Max Pos",
          type : "line",
          data : this.lineData
        }
      ],
      chart: {
        height: 350,
        width : '100%',
        type: "line",
        zoom:{
          enabled: false
        },
        events:{
          click: function(event, chartContext, config){
            console.log("fromChart", chartContext, config, event);
          }, 
          dataPointSelection : function(e, cc, config){
            //debugger;
            console.log(config)
            console.log("Clicked Event", config.w.config.series[config.seriesIndex].data[config.dataPointIndex])
          }
        }
      },
      stroke: {
        width: [0, 4]
      },
      title: {
        text: ""
      },
      dataLabels: {
        enabled: true,
        enabledOnSeries: [1]
      },
      labels:  this.labelData,
      xaxis: {
        type: "category"
      },
      yaxis: [
        {
          title: {
            text: "Time(ms)"
          }
        },
        {
          opposite: true,
          title: {
            text: "Time(ms)"
          }
        }
      ]
    }
  
   }

  ngOnInit(): void {
    console.log(this.data, this.lineData, this.labelData)  
   setTimeout(() => {
     this.iChart.updateSeries([
      {
        name : "Random Move",
        type : "column",
        data : this.data
      },
      {
        name : "Random Move - Max Pos",
        type : "line",
        data : this.lineData
      }
     ])
   }, 1500);

}
}