import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';



@Component({
  selector: 'app-system-configuration',
  templateUrl: './system-configuration.component.html',
  styleUrls: ['./system-configuration.component.css']
})
export class SystemConfigurationComponent implements OnInit {

  constructor(
    private router : Router,
    private app : AppComponent
  ){ 
    this.router.navigateByUrl('/system-configuration/users-module');
  }

  ngOnInit(): void {
  }

}
