import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { KameraiService } from '../_services/kamerai.service';

import { testData, dummyData, dummy_data } from '../_services/data.service';
import { environment } from 'src/environments/environment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ChartComponent,
ApexNonAxisChartSeries, ApexResponsive, ApexChart, ApexLegend ,
ApexAxisChartSeries, ApexFill, ApexXAxis, ApexYAxis, ApexTooltip, ApexTitleSubtitle } from 'ng-apexcharts';
import * as ApexCharts from 'apexcharts';
import { RandomMoveComponent } from '../random-move/random-move.component';

//RandomMoveComponent

export type  ChartOptionsPie =  {
  series : ApexNonAxisChartSeries,
  chart : ApexChart,
  legend : ApexLegend,
  responsive : ApexResponsive[],
  labels : any
  tooltip : ApexTooltip
}
export type  ChartOptionsPie_Summary =  {
  series : ApexNonAxisChartSeries,
  chart : ApexChart,
  legend : ApexLegend,
  responsive : ApexResponsive[],
  labels : any
  tooltip : ApexTooltip
}
export type BarChartRMoveOptions = {
  series : ApexAxisChartSeries,
  chart : ApexChart,
  xaxis : ApexXAxis,
  yaxis : ApexYAxis | ApexYAxis [],
  title : ApexTitleSubtitle,
  labels : string[],
  dataLabels : any,
  stroke : any,
  fill : ApexFill,
  tooltip : ApexTooltip
}

export type BarChartSummaryOptions = {
  series : ApexAxisChartSeries,
  chart : ApexChart,
  xaxis : ApexXAxis,
  yaxis : ApexYAxis | ApexYAxis [],
  title : ApexTitleSubtitle,
  labels : string[],
  dataLabels : any,
  stroke : any,
  fill : ApexFill,
  tooltip : ApexTooltip,
  legend : ApexLegend
}

export type BarChartWalkOptions = {
  series : ApexAxisChartSeries,
  chart : ApexChart,
  xaxis : ApexXAxis,
  yaxis : ApexYAxis | ApexYAxis [],
  title : ApexTitleSubtitle,
  labels : string[],
  dataLabels : any,
  stroke : any,
  fill : ApexFill,
  tooltip : ApexTooltip,
  legend : ApexLegend
}

export type BarChartStandOptions = {
  series : ApexAxisChartSeries,
  chart : ApexChart,
  xaxis : ApexXAxis,
  yaxis : ApexYAxis | ApexYAxis [],
  title : ApexTitleSubtitle,
  labels : string[],
  dataLabels : any,
  stroke : any,
  fill : ApexFill,
  tooltip : ApexTooltip,
  legend : ApexLegend
}
export type BarChartEBendOptions = {
  series : ApexAxisChartSeries,
  chart : ApexChart,
  xaxis : ApexXAxis,
  yaxis : ApexYAxis | ApexYAxis [],
  title : ApexTitleSubtitle,
  labels : string[],
  dataLabels : any,
  stroke : any,
  fill : ApexFill,
  tooltip : ApexTooltip,
  legend : ApexLegend
}
export type BarChartSBendOptions = {
  series : ApexAxisChartSeries,
  chart : ApexChart,
  xaxis : ApexXAxis,
  yaxis : ApexYAxis | ApexYAxis [],
  title : ApexTitleSubtitle,
  labels : string[],
  dataLabels : any,
  stroke : any,
  fill : ApexFill,
  tooltip : ApexTooltip,
  legend : ApexLegend
}
@Component({
  selector: 'app-home-live',
  templateUrl: './home-live.component.html',
  styleUrls: ['./home-live.component.css']
})


export class HomeLiveComponent implements OnInit {
  modalRef : BsModalRef;
  @ViewChild("chart_i") chart_i : ChartComponent;
  @ViewChild("fileUploader") fileUploader:ElementRef;

  public chartOptionsPie: Partial<ChartOptionsPie>;
  public chartOptionsPie_Summary : Partial<ChartOptionsPie_Summary>;
  public barChartRMoveOptions : Partial<BarChartRMoveOptions>;
  public barChartSummaryOptions : Partial<BarChartSummaryOptions>;
  public barChartWalkOptions : Partial<BarChartWalkOptions>;
  public barChartStandOptions : Partial<BarChartStandOptions>;
  public barChartEBendOptions : Partial<BarChartEBendOptions>;
  public barChartSBendOptions : Partial<BarChartSBendOptions>;
  dataLocation =  "../../Data/Output_Json";
  _viewImageID : any;
  fileUploadStatus : any;
  basicData: any;
  basicData2 : any;
  testData : any;
  lBendData : any;
  basicOptions: any;
  dataPie :any;
  data: any;
  data2 : any;
  data3 :any;
  _raw_data : any;
  _selected_file_ID : any;
  _vision_URL : any;
  _uploaded_Filename : any;
  _selected_Filename : any;
  horizontalOptions : any
  chartOptions: any;
  currentChartFileName : string;
  _Total_data_meta : any;
   _TotalData :any;
   _testPersonsData : any;
   _testPersonsID : any;
  subscription : Subscription;
  _series : any;
  filterType = "MAX_VID";
  overAllSummaryChart : any;
  sBendChart : any;
  eBendChart : any;
  rMoveChart : any;
  walkChart   : any;
  standChart : any;
  totalTime : any;
  public formData:FormData = new FormData();
  @ViewChild('modalViewImage') _modelView: TemplateRef<HTMLDivElement>;
  isProcessListActive = true;
  //config : AppConfig;
   eBendMax : any;
   eBendMin : any;
   eBendAvg : any;
   eBendMaxPos : any
   eBendMinPos : any;
   eBendTotal : any;
   
   sBendMax : any;
   sBendMin : any;
   sBendAvg : any;
   sBendMaxPos : any
   sBendMinPos : any;
   sBendTotal : any;
   
   standMax : any;
   standMin : any;
   standAvg : any;
   standMaxPos : any
   standMinPos : any;
   standTotal : any;
   
   rMoveMax : any;
   rMoveMin : any;
   rMoveAvg : any;
   rMoveMaxPos : any
   rMoveMinPos : any;
   rMoveTotal : any;
   
   walkMax : any;
   walkMin : any;
   walkAvg : any;
   walkMaxPos : any
   walkMinPos : any;
   walkTotal : any;
   pie_summary_data : any;
   fileRequest = {
      "Filename":"",
      "Process_id":"0"
    }
  processedList : any;
   constructor(
    private service : KameraiService,
    private ngxService : NgxUiLoaderService,
    private modalService : BsModalService,
   ) {

  


    this.horizontalOptions = {
      indexAxis: 'y',
      plugins: {
          legend: {
              labels: {
                  color: '#495057'
              }
          }
      },
      scales: {
          x: {
              ticks: {
                  color: '#495057'
              },
              grid: {
                  color: '#ebedef'
              }
          },
          y: {
              ticks: {
                  color: '#495057'
              },
              grid: {
                  color: '#ebedef'
              }
          }
      }
  };

  this.chartConfigs();
  

   }

 
  ngOnInit(): void {
    if(!environment.production){
      this.processedList = dummy_data;
      // console.log(this.processedList.RESPONSE);   
      //this.getProcessFileLists();

    }else{
      this.getProcessFileLists();
    }

  this.basicOptions={   
    responsive : true,
    maintainAspectRatio : true,
    onClick:function(e, items){
      //if ( items.len gth == 0 ) return; //Clicked outside any bar.
      console.log("console From Chart",  e,items)
      //clickOnChart(lastHoveredIndex);
    }
  };
this.basicData2 = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
      {
          label: 'First Dataset',
          data: [65, 59, 80, 81, 56, 55, 40],
          fill: false,
          borderColor: '#42A5F5',
          tension: .4
      },
      {
          label: 'Second Dataset',
          data: [28, 48, 40, 19, 86, 27, 90],
          fill: false,
          borderColor: '#FFA726',
          tension: .4
      }
  ]
};

}
public chartConfigs(){
  this.barChartRMoveOptions = {
    series: [
      {
        name : "Random Move",
        type : "column",
        data : []
      },
      {
        name : "Random Move - Max Pos",
        type : "line",
        data : []
      }
    ],
    
    chart: {
      height: '420px',
      width : '100%',
      type: "line",            
      zoom:{
        enabled: false
      },
      events:{
        click: (event, chartContext, config)=>{          
          console.log("fromChart", chartContext, config, event);
        }, 
        dataPointSelection : (e, cc, config)=>{         
          this.showImages({name : config.w.config.series[config.seriesIndex].name, id: config.dataPointIndex})
          console.log(this.testData)
          console.log(config)          
          console.log("Clicked Event", config.w.config.series[config.seriesIndex].data[config.dataPointIndex])
          this.ViewImageModal(this._modelView);
        }
      }
    },
    stroke: {
      width: [0, 4]
    },
    title: {
      text: ""
    },
    tooltip:{
      y: {
        formatter: function(value : any) {
          if(Number.isInteger(value)){
            return  parseInt(value) + "";
          }else{
            return  value.toFixed(2) + " (secs)";
          }
          
        }
      }
     },
    dataLabels: {
      enabled: true,
      enabledOnSeries: [1]
    },
    labels:  [],
    xaxis: {
      type: "category"
    },
    yaxis: [
      {
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
      {
        opposite: true,
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
     
    ]
  }

  this.barChartSummaryOptions = {
    series: [
      {
        name : "Random Move",
        type : "column",
        data : []
      },
      {
        name : "Random Move - Max Pos",
        type : "line",
        data : []
      }
    ],
    legend : {
      show : false
    },
    chart: {
      height: '420px',
      width : '100%',
      type: "line",                  
      zoom:{
        enabled: false
      },
      events:{
        click: (event, chartContext, config)=>{          
          console.log("fromChart", chartContext, config, event);
        }, 
        dataPointSelection : (e, cc, config)=>{         
          this.showImages({name : config.w.config.series[config.seriesIndex].name, id: config.dataPointIndex})
          console.log(this.testData)
          console.log(config)          
          console.log("Clicked Event", config.w.config.series[config.seriesIndex].data[config.dataPointIndex])
          this.ViewImageModal(this._modelView);
        }
      }
    },
    stroke: {
      width: [0, 5]
    },
    title: {
      text: ""
    },
    tooltip:{
      y: {
        formatter: function(value : any) {
          if(Number.isInteger(value)){
            return  parseInt(value) + "";
          }else{
            return  value.toFixed(2) + " (secs)";
          }
          
        }
      }
     },
    dataLabels: {
      enabled: true,
      enabledOnSeries: [5,6,7,8,9]
    },
    labels:  [],
    xaxis: {
      type: "category"
    },
    yaxis: [
      {
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
      {
        opposite: true,
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
     
    ]
  }

  this.barChartWalkOptions = {
    series: [
      {
        name : "Walk",
        type : "column",
        data : []
      },
      {
        name : "Walk - Max Pos",
        type : "line",
        data : []
      }
    ],
    legend : {
      show : false
    },
    chart: {
      height: '420px',
      width : '100%',
      type: "line",                  
      zoom:{
        enabled: false
      },
      events:{
        click: (event, chartContext, config)=>{          
          console.log("fromChart", chartContext, config, event);
        }, 
        dataPointSelection : (e, cc, config)=>{         
          this.showImages({name : config.w.config.series[config.seriesIndex].name, id: config.dataPointIndex})
          console.log(this.testData)
          console.log(config)          
          console.log("Clicked Event", config.w.config.series[config.seriesIndex].data[config.dataPointIndex])
          this.ViewImageModal(this._modelView);
        }
      }
    },
    stroke: {
      width: [0, 5]
    },
    title: {
      text: ""
    },
    tooltip:{
      y: {
        formatter: function(value : any) {
          if(Number.isInteger(value)){
            return  parseInt(value) + "";
          }else{
            return  value.toFixed(2) + " (secs)";
          }
          
        }
      }
     },
    dataLabels: {
      enabled: true,
      enabledOnSeries: [1]
    },
    labels:  [],
    xaxis: {
      type: "category"
    },
    yaxis: [
      {
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
      {
        opposite: true,
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
     
    ]
  }

  this.barChartStandOptions = {
    series: [
      {
        name : "Stand",
        type : "column",
        data : []
      },
      {
        name : "Stand - Max Pos",
        type : "line",
        data : []
      }
    ],
    legend : {
      show : false
    },
    chart: {
      height: '420px',
      width : '100%',
      type: "line",                  
      zoom:{
        enabled: false
      },
      events:{
        click: (event, chartContext, config)=>{          
          console.log("fromChart", chartContext, config, event);
        }, 
        dataPointSelection : (e, cc, config)=>{         
          this.showImages({name : config.w.config.series[config.seriesIndex].name, id: config.dataPointIndex})
          console.log(this.testData)
          console.log(config)          
          console.log("Clicked Event", config.w.config.series[config.seriesIndex].data[config.dataPointIndex])
          this.ViewImageModal(this._modelView);
        }
      }
    },
    stroke: {
      width: [0, 5]
    },
    title: {
      text: ""
    },
    tooltip:{
      y: {
        formatter: function(value : any) {
          if(Number.isInteger(value)){
            return  parseInt(value) + "";
          }else{
            return  value.toFixed(2) + " (secs)";
          }
          
        }
      }
     },
    dataLabels: {
      enabled: true,
      enabledOnSeries: [1]
    },
    labels:  [],
    xaxis: {
      type: "category"
    },
    yaxis: [
      {
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
      {
        opposite: true,
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
     
    ]
  }

  this.barChartEBendOptions = {
    series: [
      {
        name : "E-Bend",
        type : "column",
        data : []
      },
      {
        name : "E-Bend - Max Pos",
        type : "line",
        data : []
      }
    ],
    legend : {
      show : false
    },
    chart: {
      height: '420px',
      width : '100%',
      type: "line",                  
      zoom:{
        enabled: false
      },
      events:{
        click: (event, chartContext, config)=>{          
          console.log("fromChart", chartContext, config, event);
        }, 
        dataPointSelection : (e, cc, config)=>{         
          this.showImages({name : config.w.config.series[config.seriesIndex].name, id: config.dataPointIndex})
          console.log(this.testData)
          console.log(config)          
          console.log("Clicked Event", config.w.config.series[config.seriesIndex].data[config.dataPointIndex])
          this.ViewImageModal(this._modelView);
        }
      }
    },
    stroke: {
      width: [0, 5]
    },
    title: {
      text: ""
    },
    tooltip:{
      y: {
        formatter: function(value : any) {
          if(Number.isInteger(value)){
            return  parseInt(value) + "";
          }else{
            return  value.toFixed(2) + " (secs)";
          }
          
        }
      }
     },
    dataLabels: {
      enabled: true,
      enabledOnSeries: [1]
    },
    labels:  [],
    xaxis: {
      type: "category"
    },
    yaxis: [
      {
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
      {
        opposite: true,
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
     
    ]
  }

  this.barChartSBendOptions = {
    series: [
      {
        name : "S-bend",
        type : "column",
        data : []
      },
      {
        name : "S-Bend - Max Pos",
        type : "line",
        data : []
      }
    ],
    legend : {
      show : false
    },
    chart: {
      height: '420px',
      width : '100%',
      type: "line",                  
      zoom:{
        enabled: false
      },
      events:{
        click: (event, chartContext, config)=>{          
          console.log("fromChart", chartContext, config, event);
        }, 
        dataPointSelection : (e, cc, config)=>{         
          this.showImages({name : config.w.config.series[config.seriesIndex].name, id: config.dataPointIndex})
          console.log(this.testData)
          console.log(config)          
          console.log("Clicked Event", config.w.config.series[config.seriesIndex].data[config.dataPointIndex])
          this.ViewImageModal(this._modelView);
        }
      }
    },
    stroke: {
      width: [0, 5]
    },
    title: {
      text: ""
    },
    tooltip:{
      y: {
        formatter: function(value : any) {
          if(Number.isInteger(value)){
            return  parseInt(value) + "";
          }else{
            return  value.toFixed(2) + " (secs)";
          }
          
        }
      }
     },
    dataLabels: {
      enabled: true,
      enabledOnSeries: [1]
    },
    labels:  [],
    xaxis: {
      type: "category"
    },
    yaxis: [
      {
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
      {
        opposite: true,
        title: {
          text: "Time(Secs)"
        },
        labels : {
          formatter : function(val, index){
            return val.toFixed(2)
          }
        }
      },
     
    ]
  }
}
public getSelectedFile(argValues){
  let fileList: FileList = argValues.target.files;
    
  if(fileList.length > 0) {
      this._selected_Filename = "";
      let file: File = fileList[0];
      console.log(file);
      this.formData.append('uploadedFile', file, file.name);
      this._selected_Filename = file.name;

      //argValues.target.value = null;
  }
}
public uploadFile(){
  //this.formData.append('productId',"79177634-292e-4c14-a0fa-0a9c7c428150");  
  this.ngxService.start();
  this.service.uploadVideoFile(this.formData).subscribe(data=>{
    console.log("ISUploadSuccess: ",data);    
    if (data != null && data!="" && data['status'] !== 'Failed') 
    { 
      this._uploaded_Filename = data["fileName"]
      this.fileUploadStatus = "success";
      this.formData.delete('uploadedFile');
      this.fileUploader.nativeElement.value = null;
      this.ngxService.stop();
    }else{
      this.ngxService.stop();
      this.formData.delete('uploadedFile');     
      this.fileUploader.nativeElement.value = null;  
      console.log("fileName request",data);
      this.fileUploadStatus = "failed";
    }
  },error=>{
    this.fileUploadStatus = "failed";
    this.ngxService.stop();
    this.formData.delete('uploadedFile');   
    this.fileUploader.nativeElement.value = null;    
    console.log(error);
  })
}

public processVideoFile(){
  this.ngxService.start();
  this.service.getVisionURL().subscribe(data=>{
    console.log("VisionURL",data);
    this._vision_URL = data['visionUrl'];
    this.service.processVideo({Filename:this._uploaded_Filename, Process_id:"0"}, this._vision_URL).subscribe(data=>{
      console.log("ISResponseFromVision: ", data)
      if(data['Status'] === 'Success'){        
        const temp_value = data['Data'];

        //this._TotalData = data['Data'];

        this._Total_data_meta = data['Data'];

        this._testPersonsID = Object.keys(temp_value);

        this.readJSONFile(null,'WITH_INT');

        // this.generateChartData()
        // this.testData = {
        //   "PersonID":this._testPersonsID,      
        //   "EBend":  this.eBendMax,
        //   "SBend":  this.sBendMax,
        //   "RMove":  this.rMoveMax,
        //   "Walk":   this.walkMax,
        //   "Stand":  this.standMax
        // };
        // this.totalTime =  [
        //   this.eBendMax.reduce((a,b)=>a+b,0),
        //   this.sBendMax.reduce((a,b)=>a+b,0),
        //   this.rMoveMax.reduce((a,b)=>a+b,0),
        //   this.standMax.reduce((a,b)=>a+b,0),
        //   this.walkMax.reduce((a,b)=>a+b,0),
        // ]
        // this.overAllSummaryCharts ()
        // this.sBendMotionChart();
        // this.eBendMotionChart();
        // this.rMoveMotionChart();
        // this.walkMotionChart();
        // this.standMotionChart();
        // this.totalTimePieChart();
        this.isProcessListActive = false;   
        this.formData.delete('uploadedFile');
        this.getProcessFileLists();  
        this.ngxService.stop();
      }else{
        this.ngxService.stop();
        this.formData.delete('uploadedFile');
        console.log("dataFromVision: ",data);
      }
    }, error=>{
      console.log(error);
      this.ngxService.stop();
    })
  }, error => {console.log(error);  this.ngxService.stop();})
}
public closeCharts(){
  if(!environment.production){
    this.processedList = dummy_data;
  }else{
    this.getProcessFileLists();
  }  
  this.isProcessListActive = true;
}

public showSummaryPanel(e, argFile){
  e.preventDefault();
  //debugger
  this.isProcessListActive = false;
  this._selected_file_ID = argFile;
  this.readJSONFile(argFile, "WITH_INT");
}

public readJSONFile(argFile, _type){
    //e.preventDefault();
    //debugger
    console.log("from read file : ",argFile);
    this.isProcessListActive = false;  
    if(argFile !== null){
      this._raw_data = this.processedList.RESPONSE[argFile];
      this._TotalData = this.processedList.RESPONSE[argFile].Data[_type];
      this._testPersonsID = Object.keys(this._TotalData);
    }else{
      this._TotalData = this._Total_data_meta[_type];
      this._testPersonsID = Object.keys(this._TotalData);
    }
    

    this.generateChartData();
    
    this.testData = {
      "PersonID":this._testPersonsID,
      "EBend":  this.eBendMax,
      "SBend":  this.sBendMax,
      "RMove":  this.rMoveMax,
      "Walk":   this.walkMax,
      "Stand":  this.standMax
  };

  this.pie_summary_data = [this._raw_data.with_Int.max,this._raw_data.with_out_Int.max]
  
  this.totalTime =  [
    
      this.eBendMax.reduce((a,b)=>a+b,0),
      this.sBendMax.reduce((a,b)=>a+b,0),
      this.rMoveMax.reduce((a,b)=>a+b,0),
      this.standMax.reduce((a,b)=>a+b,0),
      this.walkMax.reduce((a,b)=>a+b,0)      
  ];

  this.totalTime =  this.totalTime.map(ele=> parseFloat(ele.toFixed(2)));
  
  this.overAllSummaryCharts ();
  this.sBendMotionChart();
  this.eBendMotionChart();
  this.rMoveMotionChart();    
  this.walkMotionChart();
  this.standMotionChart();
  this.totalTimePieChart();
  this.summaryPieChart();
}

public getVisionURLPort(){
  this.service.getVisionURL().subscribe(data=>{
    console.log("VisionURL",data);
    this._vision_URL = data['visionUrl'];
  }, error => console.log(error))
}

public showImages(argItems){
  console.log(argItems);
  this._viewImageID = this._vision_URL + "/video_feed/?videofile=" + this._TotalData[this._testPersonsID[argItems.id]][argItems.name.replace("-","")][this.filterType];
}

public playDemoVideo(argItems){
  console.log(argItems);
  this._viewImageID = this._vision_URL + "/video_feed/?videofile=" + argItems;
}

public openDemoModal(argItems){  
  //this._viewImageID = this._vision_URL + "/video_feed/?original_videofile=" + argItems;
  this.playDemoVideo(argItems);
  this.ViewImageModal(this._modelView);
}

public getProcessFileLists(){
  this.service.getVisionURL().subscribe(data=>{
    console.log("VisionURL",data);
    this._vision_URL = data['visionUrl'];
    this.service.getProcessedList({},this._vision_URL).subscribe(data=>{
      console.log(data);
      this.processedList = data;
      this.ngxService.stop();
    }, error=>{
      this.ngxService.stop();
      console.error(error);
    })
  }, error => {console.log(error); this.ngxService.stop()})
  this.ngxService.start();
  

}
public repeatPlayback(argID){
  console.log(argID);
  this._viewImageID = "";
  setTimeout(() => {
    this._viewImageID = argID;
  }, 3000);
  //
}
public clearItems(){
  this._viewImageID = "";
}

public updateSummaryPieCharts (argValues) {
  
  console.log(argValues)
  if(argValues.target.value === 'MAX'){
    this.filterType = "MAX_VID";
    this.pie_summary_data = [this._raw_data.with_Int.max,this._raw_data.with_out_Int.max];
    this.summaryPieChart();
   
  }else if(argValues.target.value === 'MIN'){
    this.filterType = "MIN_VID";
    this.pie_summary_data = [this._raw_data.with_Int.min,this._raw_data.with_out_Int.min]
    this.summaryPieChart();
  }else{
    this.filterType = "AVG_VID";
    this.pie_summary_data = [this._raw_data.with_Int.avg,this._raw_data.with_out_Int.avg];
    this.summaryPieChart();
  }
}

public updateSummaryCharts (argValues) {
  
  console.log(argValues)
  if(argValues.target.value === 'MAX'){
    this.filterType = "MAX_VID";
    this.testData = {
      "PersonID":this._testPersonsID,      
      "EBend":  this.eBendMax,
      "SBend":  this.sBendMax,
      "RMove":  this.rMoveMax,
      "Walk":   this.walkMax,
      "Stand":  this.standMax
   };
   this.overAllSummaryCharts () 
  }else if(argValues.target.value === 'MIN'){
    this.filterType = "MIN_VID";
    this.testData = {
      "PersonID":this._testPersonsID,      
      "EBend":  this.eBendMin,
      "SBend":  this.sBendMin,
      "RMove":  this.rMoveMin,
      "Walk":   this.walkMin,
      "Stand":  this.standMin
   };
   this.overAllSummaryCharts ()
  }else{
    this.filterType = "AVG_VID";
    this.testData = {
      "PersonID":this._testPersonsID,      
      "EBend":  this.eBendAvg,
      "SBend":  this.sBendAvg,
      "RMove":  this.rMoveAvg,
      "Walk":   this.walkAvg,
      "Stand":  this.standAvg
   };
   this.overAllSummaryCharts ();
  }
}

overAllSummaryCharts () {
  setTimeout(() => {
    this.barChartSummaryOptions.series= [
      {
        name: 'E-bend',
        type: 'bar',
        data: this.testData.EBend
    },
    {
        name: 'S-bend',
        type: 'bar',
        data: this.testData.SBend
    },
    {
        name: 'R-move',
        type: 'bar',
        data: this.testData.RMove
    },
    {
      name: 'Walk',
      type: 'bar',
      data: this.testData.Walk
    },
    {
      name: 'Stand',
      type: 'bar',
      data: this.testData.Stand
    },    
    {
      name : "E-Bend - Max Pos",
      type : "line",
      data : this.eBendMaxPos
    },
    {
      name : "S-Bend - Max Pos",
      type : "line",
      data : this.sBendMaxPos
    },
    {
      name : "Random Move - Max Pos",
      type : "line",
      data : this.rMoveMaxPos
    },{
      name : "Walk - Max Pos",
      type : "line",
      data : this.walkMaxPos
    },
    {
      name : "Stand - Max Pos",
      type : "line",
      data : this.standMaxPos
    },
    ];
    this.barChartSummaryOptions.labels = this.testData.PersonID
    
    }, 1500);   
}
sBendMotionChart(){
  this.filterType = "MAX_VID";
  setTimeout(() => {
    this.barChartSBendOptions.series= [
        {
          name : "S-bend",
          type : "column",
          data : this.testData.SBend
        },
        {
          name : "S-Bend - Max Pos",
          type : "line",
          data : this.sBendMaxPos
        }
      ];
      this.barChartSBendOptions.labels = this.testData.PersonID
    
    }, 1500);
  this.sBendChart = {
    labels: this.testData.PersonID,
    datasets: [
        {
            label: 'S-bend',
            data: this.testData.SBend,
            backgroundColor: "#8e44ad",
        }
    ]
  };
}
eBendMotionChart(){
  this.filterType = "MAX_VID";
  setTimeout(() => {
    this.barChartEBendOptions.series= [
        {
          name : "E-bend",
          type : "column",
          data : this.testData.EBend
        },
        {
          name : "E-Bend - Max Pos",
          type : "line",
          data : this.eBendMaxPos
        }
      ];
      this.barChartEBendOptions.labels = this.testData.PersonID
    
    }, 1500);

}
rMoveMotionChart(){
  this.filterType = "MAX_VID";
console.log("r_move", this.testData.RMove, this.rMoveMaxPos)
setTimeout(() => {
  this.barChartRMoveOptions.series= [
      {
        name : "R-move",
        type : "column",
        data : this.testData.RMove
      },
      {
        name : "R-Move - Max Pos",
        type : "line",
        data : this.rMoveMaxPos
      }
    ];
    this.barChartRMoveOptions.labels = this.testData.PersonID
  
  }, 1500);
 
}
walkMotionChart(){
  this.filterType = "MAX_VID";
  this.walkChart = {
    labels: this.testData.PersonID,
    datasets: [
        {
            label: 'Walk Motion',
            data: this.testData.Walk,
            backgroundColor: "#22a6b3",
        }
    ]
  };
setTimeout(() => { 

  this.barChartWalkOptions.series= [
    {
      name : "Walk",
      type : "column",
      data : this.testData.Walk
    },
    {
      name : "Walk - Max Pos",
      type : "line",
      data : this.walkMaxPos
    }
  ];
  this.barChartWalkOptions.labels = this.testData.PersonID
}, 1500);
}
standMotionChart(){
  this.filterType = "MAX_VID";
  setTimeout(() => { 

    this.barChartStandOptions.series= [
      {
        name : "Stand",
        type : "column",
        data : this.testData.Stand
      },
      {
        name : "Stand - Max Pos",
        type : "line",
        data : this.standMaxPos
      }
    ];
    this.barChartStandOptions.labels = this.testData.PersonID
  }, 1500);

}

totalTimePieChart () { 

this.chartOptionsPie = {
  series:  this.totalTime,
  chart: {
    width: 480,
    type: "pie",
    
    events:{
      click: (event, chartContext, config)=>{          
        console.log("fromChart", chartContext, config, event);
      },
      dataPointSelection : (e, cc, config)=>{         
        //this.showImages({name : config.w.config.series[config.seriesIndex].name, id: config.dataPointIndex})
        console.log(this.testData)
        console.log(config)          
        console.log("Clicked Event", config.w.config.labels[config.dataPointIndex])
        //this.ViewImageModal(this._modelView);
      } 
    }
  },  
 tooltip:{
  y: {
    formatter: function(value) {
      return value + " (secs)";
    }
  }
 },
  labels:  [
    'E-Bend',
    'S-Bend',
    'Random Move',
    'Stand',
    'Walk'
  ],

  responsive: [
    {
      breakpoint: 480,
      options: {
        chart: {
          width: 200
        },
        legend: {
          position: "bottom"
        }
      }
    }
  ],
  legend : {
    position : "bottom"
  }
};
}


summaryPieChart () { 

  this.chartOptionsPie_Summary = {
    series:  this.pie_summary_data,
    
    chart: {
      width: 480,
      type: "pie",
      //colors: ['#fff', '#000'],
    
      events:{
        click: (event, chartContext, config)=>{          
          console.log("fromChart", chartContext, config, event);
        },
        dataPointSelection : (e, cc, config)=>{         
          //this.showImages({name : config.w.config.series[config.seriesIndex].name, id: config.dataPointIndex})
          console.log(this.testData)
          console.log(config)          
          console.log("Clicked Event", config.w.config.labels[config.dataPointIndex])
          //this.ViewImageModal(this._modelView);
        } 
      },
      
    },  
    
   tooltip:{
   
   },
    labels:  [
      'With Interactions',
      'Without Interactions'
    ],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: "bottom"
          }
        }
      }
    ],
    legend : {
      position : "bottom"
    },
  
  };
  }

public generateChartData (){

    this.eBendMax =  this._testPersonsID.map((items,idx)=>{
      return this._TotalData[items].Ebend !== undefined ? this._TotalData[items].Ebend.MAX : 0
    })

    this.eBendMin =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Ebend !== undefined ? this._TotalData[items].Ebend.MIN : 0
      })

    this.eBendAvg =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Ebend !== undefined ? this._TotalData[items].Ebend.AVG : 0
      })

    this.eBendMaxPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Ebend !== undefined ? this._TotalData[items].Ebend.MAX_POS : 0
    })

    this.eBendMinPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Ebend !== undefined ? this._TotalData[items].Ebend.MIN_POS : 0
      })

    this.eBendTotal =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Ebend !== undefined ? this._TotalData[items].Ebend.TOTAL : 0
      })


    this.sBendMax =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Sbend !== undefined ? this._TotalData[items].Sbend.MAX : 0
      })

    this.sBendMin =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Sbend !== undefined ? this._TotalData[items].Sbend.MIN : 0
      })

    this.sBendAvg =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Sbend !== undefined ? this._TotalData[items].Sbend.AVG : 0
      })

    this.sBendMaxPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Sbend !== undefined ? this._TotalData[items].Sbend.MAX_POS : 0
      })

    this.sBendMinPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Sbend !== undefined ? this._TotalData[items].Sbend.MIN_POS : 0
      })

    this.sBendTotal =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Sbend !== undefined ? this._TotalData[items].Sbend.TOTAL : 0
      })


    this.standMax =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Stand !== undefined ? this._TotalData[items].Stand.MAX : 0
      })

    this.standMin =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Stand !== undefined ? this._TotalData[items].Stand.MIN : 0
      })

    this.standAvg =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Stand !== undefined ? this._TotalData[items].Stand.AVG : 0
      })

    this.standMaxPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Stand !== undefined ? this._TotalData[items].Stand.MAX_POS : 0
      })

    this.standMinPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Stand !== undefined ? this._TotalData[items].Stand.MIN_POS : 0
      })

    this.standTotal =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Stand !== undefined ? this._TotalData[items].Stand.TOTAL : 0
      })


    this.rMoveMax =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Rmove !==undefined ? this._TotalData[items].Rmove.MAX : 0
      })

    this.rMoveMin =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Rmove !== undefined ? this._TotalData[items].Rmove.MIN : 0
      })

    this.rMoveAvg =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Rmove !==undefined ? this._TotalData[items].Rmove.AVG : 0
      })

    this.rMoveMaxPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Rmove !==undefined ? this._TotalData[items].Rmove.MAX_POS : 0
      })

    this.rMoveMinPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Rmove !==undefined ? this._TotalData[items].Rmove.MIN_POS : 0
      })

    this.rMoveTotal =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Rmove !==undefined ?  this._TotalData[items].Rmove.TOTAL : 0
      })


    this.walkMax =  this._testPersonsID.map((items,idx)=>{
          return this._TotalData[items].Walk !== undefined ?  this._TotalData[items].Walk.MAX : 0         
      })

    this.walkMin =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Walk !== undefined ? this._TotalData[items].Walk.MIN : 0
      })

    this.walkAvg =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Walk !== undefined ? this._TotalData[items].Walk.AVG : 0
      })

    this.walkMaxPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Walk !== undefined ? this._TotalData[items].Walk.MAX_POS : 0
      })

    this.walkMinPos =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Walk !== undefined ? this._TotalData[items].Walk.MIN_POS : 0
      })

    this.walkTotal =  this._testPersonsID.map((items,idx)=>{
        return this._TotalData[items].Walk !== undefined ? this._TotalData[items].Walk.TOTAL : 0
      })

}


public updatePieChart (argValues){
  
  if(argValues.target.value === 'MAX'){
    this.totalTime =  [
      this.eBendMax.reduce((a,b)=>a+b,0),
      this.sBendMax.reduce((a,b)=>a+b,0),
      this.rMoveMax.reduce((a,b)=>a+b,0),
      this.standMax.reduce((a,b)=>a+b,0),
      this.walkMax.reduce((a,b)=>a+b,0),
    ]
    this.totalTime =  this.totalTime.map(ele=> parseFloat(ele.toFixed(2)));
  }
  else if(argValues.target.value === 'MIN'){
    this.totalTime =  [
      this.eBendMin.reduce((a,b)=>a+b,0),
      this.sBendMin.reduce((a,b)=>a+b,0),
      this.rMoveMin.reduce((a,b)=>a+b,0),
      this.standMin.reduce((a,b)=>a+b,0),
      this.walkMin.reduce((a,b)=>a+b,0),
    ]
    this.totalTime =  this.totalTime.map(ele=> parseFloat(ele.toFixed(2)));
  }else{
    this.totalTime =  [
      this.eBendAvg.reduce((a,b)=>a+b,0),
      this.sBendAvg.reduce((a,b)=>a+b,0),
      this.rMoveAvg.reduce((a,b)=>a+b,0),
      this.standAvg.reduce((a,b)=>a+b,0),
      this.walkAvg.reduce((a,b)=>a+b,0),
    ]
    this.totalTime =  this.totalTime.map(ele=> parseFloat(ele.toFixed(2)));
  }
  this.totalTimePieChart()
}


public updateStandChart (argValues){
  if(argValues.target.value === 'MAX'){
    this.filterType = "MAX_VID";
    this.barChartStandOptions.series= [
      {
        name : "Stand",
        type : "column",
        data : this.standMax
      },
      {
        name : "Stand - Max Pos",
        type : "line",
        data : this.standMaxPos
      }
    ];
    this.barChartWalkOptions.labels = this.testData.PersonID
   
  }
  else if(argValues.target.value === 'MIN'){
    this.filterType = "MIN_VID";
    this.barChartStandOptions.series= [
      {
        name : "Stand",
        type : "column",
        data : this.standMin
      },
      {
        name : "Stand - Min Pos",
        type : "line",
        data : this.standMinPos
      }
    ];
    this.barChartWalkOptions.labels = this.testData.PersonID
   
  }else{
    this.filterType = "AVG_VID";
    this.barChartStandOptions.series= [
      {
        name : "Stand",
        type : "column",
        data : this.standAvg
      },
      {
        name : "Stand - Total",
        type : "line",
        data : this.standTotal
      }
    ];
    this.barChartWalkOptions.labels = this.testData.PersonID

   
  }
 // this.totalTimePieChart()
}


public updateWalkChart (argValues){
  if(argValues.target.value === 'MAX'){
    this.filterType = "MAX_VID";
    this.barChartWalkOptions.series= [
      {
        name : "Walk",
        type : "column",
        data : this.walkMax
      },
      {
        name : "Walk - Max Pos",
        type : "line",
        data : this.walkMaxPos
      }
    ];
    this.barChartWalkOptions.labels = this.testData.PersonID

  }
  else if(argValues.target.value === 'MIN'){
    this.filterType = "MIN_VID";
    this.barChartWalkOptions.series= [
      {
        name : "Walk",
        type : "column",
        data : this.walkMin
      },
      {
        name : "Walk - Min Pos",
        type : "line",
        data : this.walkMinPos
      }
    ];
    this.barChartWalkOptions.labels = this.testData.PersonID
   
  }else{
    this.filterType = "AVG_VID";
    this.barChartWalkOptions.series= [
      {
        name : "Walk",
        type : "column",
        data : this.walkAvg
      },
      {
        name : "Walk - Total",
        type : "line",
        data : this.walkTotal
      }
    ];
    this.barChartWalkOptions.labels = this.testData.PersonID
   
  }
  
}


public updateRMoveChart (argValues){
  if(argValues.target.value === 'MAX'){
    this.filterType = "MAX_VID";

    this.barChartRMoveOptions.series= [
      {
        name : "R-move",
        type : "column",
        data : this.rMoveMax
      },
      {
        name : "Random Move - Max Pos",
        type : "line",
        data : this.rMoveMaxPos
      }
    ];
    this.barChartRMoveOptions.labels = this.testData.PersonID

  
  }
  else if(argValues.target.value === 'MIN'){
    this.filterType = "MIN_VID";
    this.barChartRMoveOptions.series= [
      {
        name : "R-move",
        type : "column",
        data : this.rMoveMin
      },
      {
        name : "Random Move - MIN Pos",
        type : "line",
        data : this.rMoveMinPos
      }
    ];
    this.barChartRMoveOptions.labels = this.testData.PersonID

 
  }else{
    this.filterType = "AVG_VID";
    this.barChartRMoveOptions.series= [
      {
        name : "R-move",
        type : "column",
        data : this.rMoveAvg
      },
      {
        name : "Random Move - Max Pos",
        type : "line",
        data : this.rMoveTotal
      }
    ];
    this.barChartRMoveOptions.labels = this.testData.PersonID
  
  }
  
}


public updateEBendChart (argValues){
  if(argValues.target.value === 'MAX'){
    this.filterType = "MAX_VID";
    this.barChartEBendOptions.series= [
      {
        name : "E-bend",
        type : "column",
        data : this.eBendMax
      },
      {
        name : "E-Bend - Max Pos",
        type : "line",
        data : this.eBendMaxPos
      }
    ];
    this.barChartEBendOptions.labels = this.testData.PersonID
  }
  else if(argValues.target.value === 'MIN'){
    this.filterType = "MIN_VID";
    this.barChartEBendOptions.series= [
      {
        name : "E-bend",
        type : "column",
        data : this.eBendMin
      },
      {
        name : "E-Bend - Min Pos",
        type : "line",
        data : this.eBendMinPos
      }
    ];
    this.barChartEBendOptions.labels = this.testData.PersonID
  }else{
    this.filterType = "AVG_VID";
    this.barChartEBendOptions.series= [
      {
        name : "E-bend",
        type : "column",
        data : this.eBendAvg
      },
      {
        name : "E-Bend - Total",
        type : "line",
        data : this.eBendTotal
      }
    ];
    this.barChartEBendOptions.labels = this.testData.PersonID
   
  }
  
}

public updateSBendChart (argValues){
  if(argValues.target.value === 'MAX'){
    this.filterType = "MAX_VID";
    this.barChartSBendOptions.series= [
      {
        name : "S-bend",
        type : "column",
        data : this.sBendMax
      },
      {
        name : "S-Bend - Max Pos",
        type : "line",
        data : this.eBendMaxPos
      }
    ];
    this.barChartSBendOptions.labels = this.testData.PersonID
   
  }
  else if(argValues.target.value === 'MIN'){
    this.filterType = "MIN_VID";
    this.barChartSBendOptions.series= [
      {
        name : "S-bend",
        type : "column",
        data : this.eBendMin
      },
      {
        name : "S-Bend - Min Pos",
        type : "line",
        data : this.eBendMinPos
      }
    ];
    this.barChartSBendOptions.labels = this.testData.PersonID
  }else{
    this.filterType = "AVG_VID";
    this.barChartSBendOptions.series= [
      {
        name : "S-bend",
        type : "column",
        data : this.eBendAvg
      },
      {
        name : "S-Bend - Total",
        type : "line",
        data : this.eBendTotal
      }
    ];
    this.barChartSBendOptions.labels = this.testData.PersonID
   
  }
  
}



ViewImageModal(templateType:TemplateRef<any>){
  this.modalRef= this.modalService.show(templateType,Object.assign({},{class:'modal-lg'}))
}
}
