import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CameraConfigurationComponent } from './camera-configuration.component';

describe('CameraConfigurationComponent', () => {
  let component: CameraConfigurationComponent;
  let fixture: ComponentFixture<CameraConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CameraConfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
