import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy, PathLocationStrategy} from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Router, RouterModule, Routes } from '@angular/router'
import { FormsModule } from '@angular/forms'

import { ToastrModule } from 'ngx-toastr';
import { NgxUiLoaderModule, NgxUiLoaderConfig } from 'ngx-ui-loader';
import { CalendarModule } from 'primeng/calendar';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {ChartModule} from 'primeng/chart';

import {NgApexchartsModule} from 'ng-apexcharts'

import { HasPermissionDirective } from './_directive/has-permission.directive';
import { KameraiService } from './_services/kamerai.service'
import { KameraiInterceptor } from './_interceptors/kamerai.interceptor'

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeLiveComponent } from './home-live/home-live.component';
import { ProductCatalogComponent } from './product-catalog/product-catalog.component';
import { SystemConfigurationComponent } from './system-configuration/system-configuration.component';
import { UserModuleComponent } from './user-module/user-module.component';
import { RoleGroupModuleComponent } from './role-group-module/role-group-module.component';
import { CameraConfigurationComponent } from './camera-configuration/camera-configuration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RandomMoveComponent } from './random-move/random-move.component';




//Routing
const appRoutes : Routes = [
  {
    path : '',
    component:LoginComponent    
  },
  {
    path : 'Login',
    component : LoginComponent
  },
  {
    path : 'home-live',
    component : HomeLiveComponent
  },
  {
    path : 'product-catalog',
    component : ProductCatalogComponent
  },
  {
    path : 'dashboard',
    component : DashboardComponent
  },
  {
    path : 'system-configuration',
    component : SystemConfigurationComponent,
    children :[
        {
          path : 'camera-configuration',
          component: CameraConfigurationComponent
        },
        {
          path : 'users-module',
          component : UserModuleComponent
        },
        {
          path : 'users-roles-module',
          component : RoleGroupModuleComponent
        }
    ]
  }
];

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  "bgsColor": "#01CD5C",
  "bgsOpacity": 0.5,
  "bgsPosition": "bottom-right",
  "bgsSize": 60,
  "bgsType": "square-loader",
  "blur": 5,
  "delay": 0,
  "fgsColor": "#01CD5C",
  "fgsPosition": "center-center",
  "fgsSize": 60,
  "fgsType": "square-loader",
  "gap": 24,
  "logoPosition": "center-center",
  "logoSize": 120,
  "logoUrl": "",
  "masterLoaderId": "master",
  "overlayBorderRadius": "0",
  "overlayColor": "rgba(40, 40, 40, 0.8)",
  "pbColor": "red",
  "pbDirection": "ltr",
  "pbThickness": 3,
  "hasProgressBar": true,
  "text": "",
  "textColor": "#FCFCFC",
  "textPosition": "center-center",
  "maxTime": -1,
  "minTime": 500

};

@NgModule({
  declarations: [
    AppComponent,
    HasPermissionDirective,
    LoginComponent,
    HomeLiveComponent,
    ProductCatalogComponent,
    SystemConfigurationComponent,
    UserModuleComponent,
    RoleGroupModuleComponent,
    CameraConfigurationComponent,
    DashboardComponent,
    RandomMoveComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    ToastrModule.forRoot({
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      autoDismiss:true,
      closeButton : true
    }),
    RouterModule.forRoot(appRoutes),
    CalendarModule,
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    ChartModule,
    NgApexchartsModule
  ],
  providers: [
    {
      provide : HTTP_INTERCEPTORS, 
      useClass : KameraiInterceptor,
      multi:true
    },
    { 
      provide: APP_BASE_HREF,
      useValue: ''
    },
    { 
      provide: LocationStrategy,
      useClass: PathLocationStrategy  
    }
  ],
  bootstrap: [AppComponent],
  exports : [RouterModule]
})
export class AppModule { }
