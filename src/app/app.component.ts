import { Component, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { environment } from './../environments/environment';
import { KameraiService } from './_services/kamerai.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {

//Declarations
  modalRef : BsModalRef;
  isUserLogging = false;
  currentLoggedUser = {};
  title = 'kamerai-app-ui';
  currentPage: string
  //nPasswordStrength:number = 0;



  constructor(
    private toastr : ToastrService,
    private router : Router,
    private service : KameraiService,
    private ngxService : NgxUiLoaderService,
    private modalService : BsModalService,
   
  ){

    //this.initService();
    this.isUserLogging = JSON.parse(localStorage.getItem('isUserLogin'));
    this.router.events.subscribe((res) => { 
      //console.log(this.router.url.replace("/",""));
      this.currentPage = this.router.url.replace("/","");
    })
  }
   
  ngOnInit() {
    
    // if(!this.isUserLogging){
    //   this.router.navigateByUrl("/Login");
    // }
  }

  public loginEvent(flag,url,user){
    this.isUserLogging = flag;
    this.router.navigateByUrl(url);
    this.currentLoggedUser = user;
  }

  public initService(){
    debugger
    this.ngxService.start();
    this.service.getSessionAttribute().subscribe(data => {
      if(data['status'] != null && data['status'] != undefined && data['status'] == 'SUCCESS' ){
        this.currentLoggedUser = data ['response'];
        this.isUserLogging = true;        
        this.service.setUser(this.currentLoggedUser);
        if(this.currentLoggedUser['stationPermission'] != "Global"){
          //this.setMenu('/home-live');
          this.router.navigateByUrl("/home-live");
        }else{
          //this.setMenu('/product-catalog');
          this.router.navigateByUrl("/product-catalog");
        }
        // this.notificationAlert();
      }else{
        this.isUserLogging = false;
        if(this.currentPage != 'Login'){
          //console.log('Im From check Condition');
          this.doLogout();
          //this.router.navigateByUrl("/Login");
        }
        //console.log("I'm from InitService...!", this.currentPage );
        //this.doLogout();
      }
      this.ngxService.stop();
    },error => {
      this.ngxService.stop();
    });
   }
  
  
  public doLogout(){
    this.ngxService.start();
    //this.service.logout().subscribe(data => {
      this.isUserLogging = false;
      localStorage.removeItem('rolePermissions');
      localStorage.clear();
      this.router.navigateByUrl("/Login");
      //setTimeout(function(){ location.reload(); }, 200);
      this.ngxService.stop();
    //},error => {
     // this.toastr.error(error);
      this.ngxService.stop();
    //});
    
  }


  public setAuditFields(json) {
    json['createdBy'] = this.currentLoggedUser['userName'];
    json['updatedBy'] = this.currentLoggedUser['userName'];
    json['createdDate'] = new Date();
    json['updatedDate'] = new Date();
    return json;
  }

  public updateAuditFields(json) {
    json['updatedBy'] = this.currentLoggedUser['userName'];
    json['updatedDate'] = new Date();
    return json;
  }

  public getCurrentUserId() {
    return this.currentLoggedUser['userid'];
  }

  public getCurrentUser() {
    return this.currentLoggedUser;
  }

  public changeUserPassword(templateType:TemplateRef<any>){
    this.modalRef= this.modalService.show(templateType,Object.assign({},{class:'modal-sm'}))
  }

  
  public checkPasswordStrength(password){
 
    let nPasswordStrength = 0;
   if (password.match(/(?=.{8,})/)) {// 10 digits
     //this.passwordStrenticon['eightcharacter'] = true; 
     nPasswordStrength++;
   }else{
     //this.passwordStrenticon['eightcharacter'] = false; 
     nPasswordStrength--;
   }
   if (password.match(/([0-9])/)){ // Number
     //this.passwordStrenticon['number'] = true;  
     nPasswordStrength++;
   } else{
     //this.passwordStrenticon['number'] = false;  
     nPasswordStrength--;
   }
   if (password.match(/(?=.*[a-z])/)){ // lowwercase
     //this.passwordStrenticon['lowwercase'] = true;  
     nPasswordStrength++;
   }  else{
    // this.passwordStrenticon['lowwercase'] = false;  
    nPasswordStrength--;
   }
   if (password.match(/(?=.*[A-Z])/)){ // uppercase
     //this.passwordStrenticon['uppercase'] = true;  
     nPasswordStrength++
   }  else{
     //this.passwordStrenticon['uppercase'] = false; 
     nPasswordStrength--;
   }
   if (password.match(/(?=.*[!#$%&? "])/)){ //specialcharacter
     //this.passwordStrenticon['specialcharacter'] = true; 
     nPasswordStrength++;
   } else{
     //this.passwordStrenticon['specialcharacter'] = false; 
     nPasswordStrength--;
   }
   console.log( nPasswordStrength);
}



public disablebuttonPermission(permission){
  let hasPermission = false;
    if(this.currentLoggedUser && this.currentLoggedUser['userpermissions'] != null && this.currentLoggedUser['userpermissions'].length>0){
      for(const userPermission of this.currentLoggedUser['userpermissions']){
        if(permission.toUpperCase() == userPermission.toUpperCase()){
          hasPermission = true;
        }
      }
    }
    return hasPermission;
}

public upadteAuditFields(json) {
  json['updatedBy'] = this.currentLoggedUser['userName'];
  json['updatedDate'] = new Date();
  return json;
}




















}
