import { Component, OnInit } from '@angular/core';
import {  } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { AppComponent } from '../app.component';
import { KameraiService } from '../_services/kamerai.service';

@Component({
  selector: 'app-role-group-module',
  templateUrl: './role-group-module.component.html',
  styleUrls: ['./role-group-module.component.css']
})
export class RoleGroupModuleComponent implements OnInit {

  config = {
    itemsPerPage: 10,
    currentPage: 1,
    totalItems: 0
  };

  validation = [null, undefined, 'Select', 'Un known', '-1'];
  roleGroup = {};
  roleDropdownList = [];
  roleSelectedRoleGroup = [];
  roleDropdownSettings = {};
  allRoleGroups = [];
  newrole = false;


  constructor(
    private app : AppComponent,
    private toastr : ToastrService,
    private ngxService : NgxUiLoaderService,
    private service : KameraiService
  ) { 

    this.getAllRolesGroups();
  }

  ngOnInit(): void {
  }
  public getAllRolesGroups() {
    this.ngxService.start();
    this.service.getAllRolesGroups().subscribe(data => {
      if(data != null && data != undefined)
      {
        this.allRoleGroups = data;
        this.config = {
          itemsPerPage: 10,
          currentPage: 1,
          totalItems: this.allRoleGroups.length
      };
      console.log(this.allRoleGroups);
    }
      this.ngxService.stop();
    }, error => {
      //this.toastr.error(error.message);
      this.ngxService.stop();
    });
  }
}
