import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleGroupModuleComponent } from './role-group-module.component';

describe('RoleGroupModuleComponent', () => {
  let component: RoleGroupModuleComponent;
  let fixture: ComponentFixture<RoleGroupModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoleGroupModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleGroupModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
