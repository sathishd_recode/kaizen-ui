import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';

import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { KameraiService } from '../_services/kamerai.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isUserLogging = false;
  
  constructor(
    private app : AppComponent,
    private toastr : ToastrService,
    private ngxService : NgxUiLoaderService,
    private service : KameraiService,
    private router : Router
  ) { 
    if(sessionStorage.getItem("isUserLogging") == 'true'){
      this.router.navigateByUrl('/home-live');
    }
    else{
      this.router.navigateByUrl('/Login');
      this.app.isUserLogging = false;
    }
  }
    
    userCredentials = {
      userName : '',
      password : ''
    };

  
  ngOnInit(): void {
    const _idata = require("../../../testData.json");
    console.log("DataFromfile",_idata)
  }


  public doLogin(){
    this.ngxService.start();
    if(this.userCredentials.userName == 'admin' && this.userCredentials.password == 'admin123$'){
      this.isUserLogging = true;
      localStorage.setItem('isUserLogin', JSON.stringify(this.isUserLogging));
      this.app.loginEvent(true,'/home-live','Admin');
    }
    else{
      this.toastr.error("Invalid user");
    }
    this.ngxService.stop();
    // this.service.login(this.userCredentials).subscribe(data =>{
    //   if(data['status'] == 'FAILED'){
    //     if(data['message'] != null){
    //       this.toastr.error(data['message']);
    //     }else{
    //       this.toastr.error("Invalid User");
    //     }
    //   }else{
        
    //     this.isUserLogging = true;
    //     if(data['user']['stationPermission'] != "Global"){
    //       this.app.loginEvent(true,'/home-live',data['user']);
    //     }else{
    //       this.app.loginEvent(true,'/product-catalog',data['user']);
    //     }
        
    //     this.service.setUser(data['user']);
    //   }
    //   this.ngxService.stop();
    // },
    // error=>{

    // })
  }
}
