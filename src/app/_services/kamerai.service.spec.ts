import { TestBed } from '@angular/core/testing';

import { KameraiService } from './kamerai.service';

describe('KameraiService', () => {
  let service: KameraiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KameraiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
