import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { BehaviorSubject, observable } from 'rxjs'
import { distinctUntilChanged } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class KameraiService {

  private currentUserSubject = new BehaviorSubject<any>({});
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());
  public API_URL = environment.apiURL;

  setUser(user){
    this.currentUserSubject.next(user)
  }
  
  getUser(){
    return this.currentUser;
  }
  constructor(private http: HttpClient) { }

//Default API calls
  getNotificationAlert(){
    return this.http.get<{}>(this.API_URL  + "/boxmeasure-api/boxmeasure-service/get-notification-alert");
  }

  getAllRolesGroups(){
    return this.http.get<JSON[]>(this.API_URL  + "/boxmeasure-api/role-service/get-all-roles-groups");
  }
  
  getAllDefaultRoles(){
    return this.http.get<JSON[]>(this.API_URL  + "/boxmeasure-api/role-service/default/get-all-roles");
  }  

  createUser(user){
    return this.http.post<JSON[]>(this.API_URL  + "/boxmeasure-api/user-service/user-create",user);
  }
  updateUser(user){
    return this.http.post<JSON[]>(this.API_URL  + "/boxmeasure-api/user-service/user-update",user);
  }

  getAllUser(){
    return this.http.get<JSON[]>(this.API_URL  + "/boxmeasure-api/user-service/get-all-users");
  }
  
  getUserById(userId){
    return this.http.get<JSON[]>(this.API_URL  + "/boxmeasure-api/user-service/get-user-by-id/"+userId);
  }

  changePassword(user : {}){
    return this.http.post<{}>(this.API_URL  + "/boxmeasure-api/user-service/change-user-password",user);
  }



//Login & Logout API Calls
  login(user : {}){
    return this.http.post<{}>(this.API_URL + "/api/boxmeasure-auth/sign-in",user);
  }

  logout(){
    return this.http.get<{}>(this.API_URL + "/api/boxmeasure-auth/log-out");
  }

  getSessionAttribute(){
    return this.http.get<JSON[]>(this.API_URL + "/api/boxmeasure-auth/get-session-attribute");
  }

  removeSessionAttribute(){
    return this.http.get<{}>(this.API_URL + "/api/boxmeasure-auth/remove-session-attribute");
  }

//Based On Requirement Functionalities API Calls
  
getAllBoxMeasures(boxmeasure){
  //debugger;
   return this.http.post<JSON[]>(this.API_URL  +"/boxmeasure-api/track-ref-service/get-all-tracks",boxmeasure);
 }
getLatestTrackData(){
  return this.http.get<JSON[]>(this.API_URL + "boxmeasure-api/track-ref-service/get-latest-track-data");
}

//Madrid Solution Only
getLastFiveTrackData(){
  return this.http.get<JSON[]>(this.API_URL + "boxmeasure-api/track-ref-service/get-latest-five-track-data");
}

getImageToView(trackID){
  return this.http.get<JSON[]>(`${this.API_URL}boxmeasure-api/track-ref-service/get-ref-images-by-trackid/${trackID}`)
}


getVisionURL (){
  return this.http.get<{}>("/kaizen-api/track-ref-service/get-vision-url");
}


uploadVideoFile(data){
  // return this.http.post<{}>("http://localhost:8082/kaizen-api/track-ref-service/process-file" , data, {
  //   headers: new HttpHeaders({'Content-Type': 'multipart/form-data','Accept': 'application/json'}),
  //   observe: 'response'
  // })
  let headers = new HttpHeaders();
  //this is the important step. You need to set content type as null
  headers.set('Content-Type', "multipart/form-data");
  headers.set('Accept', "multipart/form-data");
  // headers.set('Content-Length', "10M");
  // headers.set("client_max_body_size","10M");
  // headers.set("Access-Control-Allow-Methods","POST, GET, OPTIONS, PUT, DELETE");
  headers.set("Access-Control-Allow-Headers","Content-Type, Origin");
  let params = new HttpParams();
  return this.http.post<{}>("/kaizen-api/track-ref-service/process-file",data, {
     params, headers 
  }) 
}

processVideo(argValues, argURL){  
  let headers = new HttpHeaders();
  headers.set("Access-Control-Allow-Methods","POST, GET, OPTIONS, PUT, DELETE");
   headers.set("Access-Control-Allow-Headers","Content-Type, Origin");
   let params = new HttpParams();
  return this.http.post<{}>( argURL +"/kaizen_process/",argValues, {
    params, headers
  });
}

getProcessedList(argvalues, argURL){
  console.log(argURL);
  let headers = new HttpHeaders();
  headers.set("Access-Control-Allow-Methods","POST, GET, OPTIONS, PUT, DELETE");
  headers.set("Access-Control-Allow-Headers","Content-Type, Origin");
  let params = new HttpParams();
  return this.http.post<{}>(argURL +"/list_of_processed_items/",{},{
    params, headers
  });
}










































  
}
