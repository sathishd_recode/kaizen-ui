import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { KameraiService } from '../_services/kamerai.service';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AppComponent } from '../app.component';
import { Router } from '@angular/router'
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription, timer } from 'rxjs';


@Component({
  selector: 'app-product-catalog',
  templateUrl: './product-catalog.component.html',
  styleUrls: ['./product-catalog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProductCatalogComponent implements OnInit {

  selectedDateTime = {
    startDate : new Date(),
    endDate : new Date()
  }
  tableSort={
    key: "createdDate",
    type: "desc"
  }
  getProductDetails = {
    "pageNumber": 0,
    "totalSize": 0,
    "limit": 10,
    "sort": this.tableSort,   
    "startDate": this.selectedDateTime.startDate,
    "endDate": this.selectedDateTime.endDate,
    "statusFilter": "All Records",
    "trackData": null,
  }
  config ={
    itemsPerPage : 10,
    currentPage : 1,
    totalItems : 0
  }
  allboxmeasure = [];
  allBarcodevalues = [];
  selectedboxmeasure = {};

  constructor(
    private service : KameraiService,
    private toastr : ToastrService,
    private ngxService : NgxUiLoaderService,
    private app : AppComponent
  ) {

    const currentDateTime = new Date();
          currentDateTime.setHours(0,0,0,0);
          this.selectedDateTime.startDate = currentDateTime;
   }
 
  ngOnInit(): void {
    setInterval(() => {
      var currentdate = new Date();
      var startDate = new Date(this.selectedDateTime.startDate);
      var endDate = new Date(this.selectedDateTime.endDate);
      if (startDate.getDate() == currentdate.getDate() &&
        (startDate.getMonth() + 1) == (currentdate.getMonth() + 1) &&
        startDate.getFullYear() == currentdate.getFullYear() &&
        endDate.getDate() == currentdate.getDate() &&
        (endDate.getMonth() + 1) == (currentdate.getMonth() + 1) &&
        endDate.getFullYear() == currentdate.getFullYear()) {
        this.selectedDateTime.endDate = new Date();
        this.getProductDetails.startDate = this.selectedDateTime.startDate;
        this.getProductDetails.endDate = this.selectedDateTime.endDate;
      
        this.service.getAllBoxMeasures(this.getProductDetails).subscribe(data => {
          console.log(data);
          this.allboxmeasure = data['trackData'];
          this.allBarcodevalues = data['trackData']['barcode'];
          this.getProductDetails.totalSize = data['totalSize'];
          this.getProductDetails.pageNumber = data['pageNumber'];
          this.config.totalItems = data['totalSize'];
        }, error => {
          //this.toastr.error(error.message);
        });
      }
    }, 10000);

  }

}
