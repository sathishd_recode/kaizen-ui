import { Injectable, Injector } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { KameraiService } from '../_services/kamerai.service';
import { map, catchError } from 'rxjs/operators';
// import { AppComponent } from '../app.component';

@Injectable()
export class KameraiInterceptor implements HttpInterceptor {
  user = {};
  constructor(
    private service : KameraiService, 
    private router : Router
    // private app : AppComponent  
  ){}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const clonedRequest = request.clone({
      withCredentials : true
    });

    return next.handle( clonedRequest ).pipe(
      map((event: HttpEvent<any>)=>{
        if(event instanceof HttpResponse){

        }
        return event;
      }), catchError(error =>{
        console.log("Error responsive status: ", error.status);
        if(error.status === 401 || error.status === 0 || error.status === 404 ) {
          this.router.navigateByUrl("/Login");      
          console.error("I'm from Interceptor!");    
        }
        return throwError(error);
      }));
  }



















}