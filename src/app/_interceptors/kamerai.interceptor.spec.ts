import { TestBed } from '@angular/core/testing';

import { KameraiInterceptor } from './kamerai.interceptor';

describe('KameraiInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      KameraiInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: KameraiInterceptor = TestBed.inject(KameraiInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
