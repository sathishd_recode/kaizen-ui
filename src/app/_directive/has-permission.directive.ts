import { Directive, Input, TemplateRef, ViewContainerRef, ElementRef, OnInit, Attribute } from '@angular/core';
import { KameraiService } from '../_services/kamerai.service';

@Directive({
  selector: '[appHasPermission]'
})

export class HasPermissionDirective implements OnInit{
private currentUser ;
private permissions : any;

  constructor(
    private element : ElementRef<any>,
    private templateRef : TemplateRef<any>,
    private viewContainer : ViewContainerRef,
    private userService : KameraiService
  ) { }

ngOnInit(){
  this.userService.currentUser.subscribe(user=>{
    this.currentUser = user;
    this.updateView();
  })
}

@Input()
set appHasPermission(val){
  this.permissions = val;
  this.updateView();
}

private updateView(){
  if(this.checkPermission()){
      this.viewContainer.createEmbeddedView(this.templateRef)
  }
  else{
    this.viewContainer.clear()
  }
}


private checkPermission(){
  let appHasPermission = false;
  if(
      this.currentUser && this.currentUser['userpermissions'] != null &&
      this.currentUser['userpermissions'].length >0
    ){
        for(const userPermission of this.currentUser['userpermissions']){
          const permissionFound = this.permissions.find(x=>x.toUpperCase() === userPermission.toUpperCase());

          if(permissionFound){
            appHasPermission = true;
          }
        }
  }
  return appHasPermission;
}



































}
