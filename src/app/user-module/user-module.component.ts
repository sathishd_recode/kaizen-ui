import { Component, OnInit } from '@angular/core';
import {  } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import {  IDropdownSettings } from 'ng-multiselect-dropdown'
import { KameraiService } from '../_services/kamerai.service';

import { AppComponent } from '../app.component'

@Component({
  selector: 'app-user-module',
  templateUrl: './user-module.component.html',
  styleUrls: ['./user-module.component.css']
})
export class UserModuleComponent implements OnInit {

  isUserCreateWritePermission = false;
  isUserEditWritePermission = false;
  userSearch = "";

  public maxSize: number = 0;
  public directionLinks: boolean = true;
  public autoHide: boolean = false;
  public responsive: boolean = true;
  public labels: any = {
    previousLabel: '',
    nextLabel: '',
    screenReaderPaginationLabel: 'Pagination',
    screenReaderPageLabel: 'page',
    screenReaderCurrentLabel: `You're on page`
  };


  allUsers = [];
  allRolesgroups = [];
  config : {
    itemsPerPage : 10,
    currentPage : 1,
    totalItems : number
  }

  validation = [null, undefined, 'Select', 'Un known', '-1'];
  user = {};  
  userSelectedRoleGroup = [];
  userDropdownSettings = {};
  submenuitem = "User Landing";

  isNewUser = false;
  isUpdateUser = false;

  constructor(
    private toastr : ToastrService,
    private ngxService : NgxUiLoaderService,
    private service : KameraiService,
    private app : AppComponent,
   

  ) { 
    this.getAllUsers();
    this.getAllRolesGroups();
    


    setTimeout(() => {    //<<<---    using ()=> syntax
      this.isUserCreateWritePermission = this.app.disablebuttonPermission("User_Create_Write");
      this.isUserEditWritePermission = this.app.disablebuttonPermission("User_Edit_Write");
    }, 1000);
  }
  


  dropdownList = [];
  selectedItems = [];
  dropdownSettings : IDropdownSettings;
  ngOnInit() {
    this.dropdownList = [
     
    ];
    this.selectedItems = [
      // { item_id: 3, item_text: 'Pune' },
      // { item_id: 4, item_text: 'Navsari' }
    ];
    this.dropdownSettings  = {
      singleSelection: false,
      idField: 'roleGroupid',
      textField: 'roleGroupName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }
  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  // ngOnInit(): void {
  // }



  public createUserButton(type, userid, template) {
    if (type == 'New User') {
      this.isNewUser = true;
      this.isUpdateUser=false;
      this.getAllRolesGroups();
      this.userSelectedRoleGroup = [];
      this.user = {};
    } else if (type == 'Edit User') {
      this.isUpdateUser = true;
      this.isNewUser = false;
      this.getAllRolesGroups();
      this.getUserById(userid);
    } 

  }

  public getAllUsers() {
    this.ngxService.start();
    this.service.getAllUser().subscribe(data => {
      if(data != null && data != undefined)
      {
        const json: any[] = data;
        this.allUsers = json;
        this.config = {
          itemsPerPage: 10,
          currentPage: 1,
          totalItems: this.allUsers.length
      };
      console.log(this.allUsers);
    }
      this.ngxService.stop();
    }, error => {
      //this.toastr.error(error.message);
      this.ngxService.stop();
    });
  }

    
  public getUserById(userId) {
    this.ngxService.start();
    this.service.getUserById(userId).subscribe(data => {
      this.user = data;
      if (data['userRoleGroup'].length > 0) {
        this.userSelectedRoleGroup = data['userRoleGroup'][0]['rolesGroup'];
      }
      this.ngxService.stop();
    }, error => {
      //this.toastr.error(error.message);
      this.ngxService.stop();
    });
  }

  public getAllRolesGroups() {
    this.ngxService.start();
    this.service.getAllRolesGroups().subscribe(data => {
      if(data != null && data != undefined)
      {
        this.allRolesgroups = data;
      } 
      this.ngxService.stop();
    }, error => {
      //this.toastr.error(error.message);
      this.ngxService.stop();
    });
  }
  
  public pageChanged(event) {
    this.config.currentPage = event;
  }
  
  public cancelUserButton() {
    this.isNewUser = false;
    this.isUpdateUser = false;
    this.getAllUsers();
  }

  public updateUser() {
    this.ngxService.start();
    if (!this.validateUsermandatoryFields()) {
      this.user = this.app.upadteAuditFields(this.user);

      this.user['userRoleGroup'][0] = this.app.upadteAuditFields(this.user['userRoleGroup'][0]);
      var existingroles = [];
      for (let u of this.user['userRoleGroup'][0]['rolesGroup']) {
        existingroles.push(u['roleGroupid']);
      }
      this.user['userRoleGroup'][0]['rolesGroup'] = [];
      for (let ur of this.userSelectedRoleGroup) {
        for (let rg of this.allRolesgroups) {
          if (rg['roleGroupid'] == ur['roleGroupid']) {
            this.user['userRoleGroup'][0]['rolesGroup'].push(rg);
          }
        }
      }
      this.service.updateUser(this.user).subscribe(data => {
        if (data != null && data['status'] == 'SUCCESS') {
          this.toastr.success(data['message']);
        } else {
          this.toastr.error(data['message']);
        }
        this.cancelUserButton();
        this.ngxService.stop();
      }, error => {
        //this.toastr.error(error.message);
        this.ngxService.stop();
      });

    }

  }
  public createNewUser() {
    
    if (!this.validateUsermandatoryFields()) {
      this.user = this.app.setAuditFields(this.user);
      var userRoleGroup = {
        "description": this.user['firstName'] + " new Role Groups",
        "rolesGroup": []
      }
      userRoleGroup = this.app.setAuditFields(userRoleGroup);
      for (let ur of this.userSelectedRoleGroup) {
        for (let rg of this.allRolesgroups) {
          if (rg['roleGroupid'] == ur['roleGroupid']) {
            userRoleGroup.rolesGroup.push(rg);
          }
        }
      }
      this.user['userRoleGroup'] = [];
      this.user['userRoleGroup'].push(userRoleGroup);

      this.service.createUser(this.user).subscribe(data => {
        if (data != null && data['status'] == 'SUCCESS') {
          this.toastr.success(data['message']);
        } else {
          this.toastr.error(data['message']);
        }
        this.cancelUserButton();
        this.ngxService.stop();
      }, error => {
        //this.toastr.error(error.message);
        this.ngxService.stop();
      });

    }

  }

  public validateUsermandatoryFields() {
    if (this.user['firstName'] == null || this.user['firstName'] == undefined) {
      this.toastr.error("Please enter First name");
      return true;
    }
    if (this.user['lastName'] == null || this.user['lastName'] == undefined) {
      this.toastr.error("Please enter Last Name");
      return true;
    }
    if (this.user['email'] == null || this.user['email'] == undefined) {
      this.toastr.error("Please enter Email ID");
      return true;
    }
    if (this.userSelectedRoleGroup.length == 0) {
      this.toastr.error("Please select user role group");
      return true;
    }
    return false;
  }

}
